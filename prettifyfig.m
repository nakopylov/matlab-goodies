function [] = prettifyfig()
f = gcf;
f.Visible = 'on';
f.Units = 'centimeters';
ratio = sqrt(2);
w = 20 * ratio;
h = 20;
f.Position = [1, 1, w, h];
f.PaperPositionMode = 'auto';

% Legend
l = legend('show');
%l.Interpreter = 'latex';
l.FontSize = 20;
l.EdgeColor = ones(1, 3) * 0.5;

% Axes config
%axis([0, pi, -15, 0]);
ax = gca;
ax.Box = 'off';
ax.FontSize = 20;
ax.Units = 'normalized';
ax.Position = [0.14, 0.14, 0.755, 0.755];
% Grid config
grid 'off';
ax.GridLineStyle = ':';
%ax.XGrid = 'on';
ax.XMinorGrid = 'off';
%ax.YGrid = 'on';
ax.YMinorGrid = 'off';
ax.GridAlphaMode = 'manual';
ax.GridAlpha = 0.1;
%ax.TickLabelInterpreter = 'latex';
ax.Units = 'normalized';
ax.Position = [0.13, 0.2, 0.755, 0.755];

% Crop plot margins
outerpos = ax.OuterPosition;
ti = ax.TightInset;
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2) * 0.5;
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left, bottom, ax_width, ax_height];

end

